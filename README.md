# NF18-Projet Louvre

## Titre du projet
Gestion du musée du Louvre

## Descripition
C'est un projet destiné à concevoir un SGBDR des œuvres et des expositions du musée du Louvre et à développer une interface d'application permettant l'interaction avec ce système.

## Membres du groupe
 Prenom|Nom|Email
:---:|:---:|:---:
Xia|QI|qi.xia@etu.utc.fr
Bastien|LE CALVE|bastien.le-calve@etu.utc.fr
Jules|YVON|jules.yvon@etu.utc.fr
Jinshan|GUO|jinshan.guo@etu.utc.fr

## Processus du projet
Etape|Date limite rendue|Etat
:---:|:---:|:---:
Rédaction de NDC|Avant *TD7*(Fait en *TD5*)|Fini
Créaton de MCD en PlantUML|Avant *TD6*|Fini
Transformation en MLD|Avant *TD7*|Fini
SQL CREATE – INSERT|Avant *TD8*|Fini
SQL SELECT - VIEW|Avant *TD11*|Fini
NoSQL/JSON, MLD non relationnel et application Python|Avant *Semaine Finale*|Fini